
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

const express = require("express");
const bodyParser = require("body-parser");

const requiredClientController = new require("./public/controllers/client.controller");
const clientController = new requiredClientController.ClientController();

const requiredPolicyController = new require("./public/controllers/policy.controller");
const policyController = new requiredPolicyController.PolicyController();

const path = require("path");
const cors = require("cors");

const options = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: ["http://127.0.0.1:4200/", "http://localhost:4200"],
    preflightContinue: false
};
    
const main = express();

main.options("*", cors(options));
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));


main.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Origin', '*');    
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});

main.use('/client', clientController.GetRouter());
main.use('/policy',  policyController.GetRouter());

//main.listen(5000);
main.listen(process.env.PORT || 3000, function () {
    console.log("IT IS WORKING...");
});
