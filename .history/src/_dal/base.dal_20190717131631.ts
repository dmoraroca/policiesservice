import { Base } from '../pocos/base.poco';
import * as mongoose from 'mongoose';
import { Resposta } from '../pocos/resposta.poco';
import { BLL_Coneixements } from '../_bll/coneixements.bll';


export const dataSchema = new mongoose.Schema({});

const options = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 30, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
  };

/**
 * 
 */
export class  DAL_Base  
{      
    private general = null;
    public uri: string = "";




    /**
     * 
     * @param cultura 
     */
    public Get<T extends Base>(cultura: string, coleccio: string): Promise<Resposta>
    {        
        var that = this;

        return new Promise((resolve, reject) => {
            try 
            {
                console.log("host: " + that.uri);

                mongoose.connect(that.uri, options).then(() =>  {
                    //var data = mongoose.model(coleccio, dataSchema);
                    //var data = conn.db.collection(coleccio);

                    console.log("conected");

                    var action = function (err, collection) {

                        console.log("cultura " + cultura);
                        console.log("data in " + coleccio);
                        // Locate all the entries using find
                        collection.find({"Cultura": cultura}).toArray(function(err, results) {
                            let resp = new Resposta;

                            results = JSON.parse(JSON.stringify(results));

                            if (results != null) console.log("query with results in collection: " + coleccio + " with length: " + results.length );

                            resp.coleccio = coleccio;
                            resp.ok = true;

                            if (err != null) {
                                throw err;
                            }
                            else
                            {    
                                switch (coleccio)
                                {
                                    case "coneixements":
                                    resp.data = BLL_Coneixements.TractarDades(results);
                                        break;
                                    case "capcalera":                                
                                        resp.data = results[0];
                                        break;
                                    default:
                                        resp.data = results;
                                }                        
        
                                if (resp.data == null) {
                                    resp.ok = false;
                                    resp.data = "No hi han dades per la colecció [" + coleccio + "] ...";
                                } 
                            }

                            mongoose.connection.close(function () {
                                console.log('Mongoose default connection with DB :' + db_server + ' is disconnected through app termination');
                                process.exit(0);
                              });

                            resolve(resp);
                        });
                    };
                    
                    mongoose.connection.db.collection(coleccio, action);
                })
                .catch((err) => {
                    let resp = new Resposta;
                    resp.data = err;
                    resp.ok = false;

                    mongoose.connection.close(function () {
                        console.log('Mongoose default connection with DB :' + db_server + ' is disconnected through app termination');
                        process.exit(0);
                      });

                    reject(resp);             
                })
            }
            catch (ex)
            {
                let resp = new Resposta;
                resp.data = ex;
                resp.ok = false;

                mongoose.connection.close(function () {
                    console.log('Mongoose default connection with DB :' + db_server + ' is disconnected through app termination');
                    process.exit(0);
                  });

                reject(resp);         
            }
        });

    }
}