
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

const express = require("express");
const bodyParser = require("body-parser");
const requiredDataController = new require("./public/controllers/data-controller");
const requiredLoginController = new require("./public/controllers/login-controller");
const dataController = new requiredDataController.DataController();
const loginController = new requiredLoginController.LoginController();
const path = require("path");
const cors = require("cors");

const options = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: ["http://127.0.0.1:4200/", "http://localhost:4200", "http://dmoraroca.com/", "http://www.dmoraroca.com"],
    preflightContinue: false
};
    
const main = express();

main.options("*", cors(options));
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));


main.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Origin', '*');    
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});



main.use('/data', dataController.GetRouter());
main.use('/login',  loginController.GetRouter());

    

//main.listen(5000);
main.listen(process.env.PORT || 3000, function () {
    console.log("S'HA EXECUTAT CORRECTAMENT...");
});
