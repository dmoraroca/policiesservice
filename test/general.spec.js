var supertest = require("supertest");
var should = require("should");
var describe = require('mocha').describe

// UNIT test begin

describe("Test Client",function(){

  // #1 should return home page

  it("Testing uri",function(done){

    // calling home page api
     supertest.agent("http://localhost:3000")
    .get("/client")
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      //res.status.should.equal(200);
      
      done();
    });
    
  });

  it("Testing id:[e8fd159b-57c4-4d36-9bd7-a59ca13057bb] exists",function(done){

    // calling home page api
     supertest.agent("http://localhost:3000")
    .get("/client/e8fd159b-57c4-4d36-9bd7-a59ca13057bb")
    .auth('Whitley', '+Axa...')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      res.body.ok.should.equal(true);
      res.body.data.name.should.equal("Manning");
      
      done();
    });
    
  });

  it("Testing name:[Manning] exists",function(done){

    // calling home page api
     supertest.agent("http://localhost:3000")
    .get("/client/getbyname/Manning")
    .auth('Whitley', '+Axa...')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      res.body.ok.should.equal(true);
      res.body.data.id.should.equal("e8fd159b-57c4-4d36-9bd7-a59ca13057bb");
      
      done();
    });
  });
});


describe("Test Policy",function(){

  // #1 should return home page

  it("Testing uri",function(done){

    // calling home page api
     supertest.agent("http://localhost:3000")
    .get("/policy")
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      //res.status.should.equal(200);
      
      done();
    });
    
  });

  it("Testing id:[64cceef9-3a01-49ae-a23b-3761b604800b] come back one name",function(done){

    // calling home page api
     supertest.agent("http://localhost:3000")
    .get("/policy/64cceef9-3a01-49ae-a23b-3761b604800b")
    .auth('Whitley', '+Axa...')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      res.body.ok.should.equal(true);
      res.body.data.should.equal("Manning");
      
      done();
    });
    
  });

  it("Testing name:[Manning] has policies",function(done){

    // calling home page api
     supertest.agent("http://localhost:3000")
    .get("/policy/getpoliciesbyusername/Manning")
    .auth('Whitley', '+Axa...')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      //res.status.should.equal(200);

     res.body.ok.should.equal(true);
      var length = res.body.data.length > 0;
      length.should.equal(true);
      
      done();
    });
  });
});
