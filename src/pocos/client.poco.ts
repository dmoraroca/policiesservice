export class Client {
    public id: string;
    public name: string;
    public email: string; 
    public role: string;

    constructor () {
        this.id = "";     
        this.email = "";
        this.name = "";
        this.role = "";
    }
}