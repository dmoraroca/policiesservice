export class Response {
    public operation: string;
    public ok: boolean;
    public data: any;   
}