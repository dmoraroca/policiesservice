export class Policy {
    public id:string;
    public amountInsured: number;
    public email: string;
    public inceptionDate:Date;
    public installmentPayment:boolean;
    public clientId: string;

    constructor () {
        this.id = "";
        this.amountInsured = 0;
        this.email = "";
        this.inceptionDate = new Date();
        this.installmentPayment = false;
        this.clientId = "";
    }
}