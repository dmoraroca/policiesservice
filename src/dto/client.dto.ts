import { Client } from "../pocos/client.poco";
import { Response } from "../pocos/Response.poco";
import { response } from "express";

const got  = require('got');
const classNamne = "Client";

export class  ClientDto {
    public getAll = ():Promise<Response> => {
        return new Promise((resolve, reject) => {
            let resp = new Response();

            resp.operation = classNamne;            

            got('http://www.mocky.io/v2/5808862710000087232b75ac', { json: true }).then(response => {
                resp.ok = true;
                resp.data = response.body.clients;

                resolve(resp);
            }).catch(error => {
                resp.ok = false;
                resp.data = error;

                resolve(resp);                
            });
        });
    }

}