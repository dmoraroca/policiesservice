import { Policy } from "../pocos/policy.poco";
import { Response } from "../pocos/Response.poco";

const got = require("got");
const classNamne = "Policy";

export class  PolicyDto {
    public getAll = ():Promise<Response> => {
        return new Promise((resolve, reject) => {
            let resp = new Response();

            resp.operation = classNamne;            

            got('http://www.mocky.io/v2/580891a4100000e8242b75c5', { json: true }).then(response => {
                resp.ok = true;
                resp.data = response.body.policies;

                resolve(resp);
            }).catch(error => {
                resp.ok = false;
                resp.data = error;

                resolve(resp);                
            });
        });
    }

}