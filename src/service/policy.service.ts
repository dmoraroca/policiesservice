import { PolicyDto } from "../dto/policy.dto";
import { Response } from "../pocos/response.poco";
import { ClientService } from "./client.service";
import { Policy } from "../pocos/policy.poco";
import { Client } from "../pocos/client.poco";

const className = "Policy";

export class  PolicyService {
    public _dto = new PolicyDto();
    public _clientService = new ClientService();
    
    /**
     * Get all policies
     */
    public getAll = (): Promise<Response> => {
        return new Promise((resolve, reject) => {
            this._dto.getAll().then(resp => {
                resolve(resp as Response);
            }).catch((err) => {
                reject(err);
            });            
        });
    }

    /**
     * Get all policies fron one user (selected by name)
     */
    public getPoliciesByUserName = (name: string) => {       
        return new Promise((resolve, reject) => {

            this._dto.getAll().then(resPolicies => {
                this._clientService.getByName(name).then((resCS) => {
                    let resp = new Response();
                    resp.data = "";
                    resp.ok = true;        
                    resp.operation = className;

                    if (resCS.ok && resPolicies.ok) {
                        let clientId: string = (<Client>resCS.data).id;                         
                        var data: Array<Policy> = JSON.parse(JSON.stringify(resPolicies.data)) ;
                                                
                        let resFilter: Array<Policy> = data.filter((x) => (x.clientId).trim().toLowerCase()  == clientId.trim().toLowerCase());

                        if (resFilter == null || resFilter.length == 0) {
                            resp.ok = false;
                            resp.data = "No policies found for this user:" + name + ".";
                        } else {
                            resp.ok = true;
                            resp.data = resFilter;
                        }
                    } else {               
                        resp.ok = false;
                        if (!resPolicies.ok) {
                            resp.data = "No policies found for this user:" + name + ".";
                        } else if (!resCS.ok) {
                            resp.data = "The user doesn't exist.";
                        }
                        
                    }

                    resolve(resp);
                });
            }).catch((err) => {
                let resp = new Response();
                
                resp.ok = false;        
                resp.operation = className;                
                resp.data = err;

                reject(resp);
            });            
        });
    }

    public get = (id: string) => {
        let resp = new Response();
        resp.operation = className;
        return new Promise((resolve, reject) => {
            this._dto.getAll().then(res => {
                let auxData:Array<Policy> =  JSON.parse(JSON.stringify(res.data));
                let resFind = auxData.find(x => x.id == id);

                resp.ok = true;

                if (resFind) {
                    this._clientService.get(resFind.clientId)
                        .then(resClient => {
                            if (resClient.ok) {
                                resp.ok = true;
                                resp.data = resClient.data.name;
                                resolve(resp);
                            } else {
                                resp.ok = false;
                                resp.data = "Client Id not found.";
                                resolve(resp);
                            }
                        })
                        .catch((err) => {});
                    
                } else {
                    resp.ok = false;
                    resp.data = "Policy Id not found.";
                    resolve(resp);
                }


            }).catch((err) => {
                reject(err);
            });            
        });
    }
}
