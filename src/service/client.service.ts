import { ClientDto } from "../dto/client.dto";

import { Response } from "../pocos/response.poco";
import { Client } from "../pocos/client.poco";

const className = "Client";

export class  ClientService {
    private _dto = new ClientDto();

    
    /** 
     * We get all the client
    */
    public getAll = (): Promise<Response> => {
        return new Promise((resolve, reject) => {
            this._dto.getAll().then(resp => {
                resolve(resp);
            }).catch((err) => {
                reject(err);
            });            
        });
    }

    /**
     * Service get I response with one client (selected by id)
     */
    public get = (id: string): Promise<Response> => {
        
        return new Promise((resolve, reject) => {            
            let resp = new Response();
            resp.operation = className;

            this.getAll()
                .then(res => {
                    let auxData:Array<Client> =  JSON.parse(JSON.stringify(res.data));
                    let resFind = auxData.find(x => x.id == id);

                    resp.ok = true;

                    if (resFind) {
                        resp.data = resFind;
                    } else {
                        resp.ok = false;
                        resp.data = "Client Id not found.";
                    }

                    resolve(resp);
                })
                .catch(err => {
                    resp.ok = false;
                    resp.data = JSON.stringify(err);

                    reject(resp);
                });
        });        
    }

    /**
     * Service getByName I response with one client (selected by name)
     */
    public getByName = (name: string): Promise<Response> => {
        let resp = new Response();
        return new Promise((resolve, reject) => {
            this._dto.getAll()
                .then(res => {

                    let auxData:Array<Client> =  JSON.parse(JSON.stringify(res.data));
                    let resFind = auxData.find(x => x.name.trim().toLowerCase() == name.trim().toLowerCase());
                    resp.ok = true;

                    if (resFind) {
                        resp.data = resFind;
                    } else {
                        resp.ok = false;
                        resp.data = "Client Name not found.";
                    }

                    resolve(resp);
                })
                .catch(err => {
                    reject(err);
                });
        });        
    }
}