import { ClientService } from "./client.service";
import { Response } from "../pocos/response.poco";


const cPASSWORD = "+Axa...";
const className = "Login";

export class LoginService {
    private _clientService: ClientService = new ClientService();

    public isAuthentificated = (req, res, permisions: string): Promise<Response> => {
        return new Promise<Response>((resolve, reject) => { 
            if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
                let resp = new Response();

                resp.ok = false;
                resp["code"] = "401";
                resp.data = "Missing authoritation basic";
                
                resolve(resp);
                
            } else {
                const base64Credentials =  req.headers.authorization.split(' ')[1];
                const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
                const [username, password] = credentials.split(':');
                
                this.isUserFound(username, password, permisions).then(resUser => {
                    let resp = new Response();
                                        
                    resp.operation = className;

                    if (resUser.ok) {
                        resp.ok = true;
                        resp.data = "succesful logged";
                    } else {
                        resp.ok = false;
                        resp.data = resUser.data;
                    }

                    resolve(resp);
                }).catch(err => {
                    let resp = new Response();
                    
                    resp.operation = className;
                    resp.ok = false;
                    resp.data = err;

                    reject(resp);
                });
            }
        });
       

    }

    public isUserFound = (username: string, password: string, permisions: string): Promise<Response>  => {
        var resp = new Response();        

        return new Promise<Response>((resolve, reject) => { 
            if (password != cPASSWORD) {
                let resp = new Response();

                resp.ok = false;
                resp.operation = className
                resp.data = "User or Password incorrect";

                resolve(resp);
            } else {
                this._clientService.getByName(username).then(resClient => {                    
                    if (resClient.ok) {                   
                        if (resClient.data.name.trim().toLowerCase() == username.trim().toLowerCase()) {
                                                        
                            if (permisions.indexOf(resClient.data.role) >= 0) {
                                let resp = new Response();

                                resp.ok = true;
                                resp.operation = className;
                                resp.data = "client ok"    

                                resolve(resp);
                            } else {
                                let resp = new Response();

                                resp.ok = false;
                                resp.operation = className;
                                resp.data = "No user permisions.";                                

                                resolve(resp);
                            }
                        } else {
                            let resp = new Response();

                            resp.ok = false;
                            resp.operation = className
                            resp.data = "User or Password incorrect";

                            resolve(resp);
                        }
                        
                    } else {
                        let resp = new Response();

                        resp.ok = false;
                        resp.operation = className
                        resp.data = "User or Password incorrect";

                        resolve(resp);
                    }
                }).catch(err => {
                    let resp = new Response();

                    resp.operation = className;
                    resp.data = err;   
                    resp.ok = false;

                    reject(resp);
                });
            }
        });
    }  
}