import { Router } from "express";
import { PolicyService } from "../service/policy.service";
import { LoginService } from "../service/login.service";

export class PolicyController
{
  private _service = new PolicyService();
  private _loginService = new LoginService()
  private _router: Router

  constructor() {
    this._router = Router();
    this.setRouterController();
    
  }

  /**
   * Get router for policies
   */  
  GetRouter() {
    return this._router;
  }

  /**
   *
   * Routing for Policies
   *  
   */
  private setRouterController() {      
    /**
     * Testing if Policy api working for GET
     */
    this._router.post("/", (req, resp) => { 
      resp.send("POLICY CONTROLLER  (POST)")   
    });

    /**
     * Testing if Policy api working for POST
     */
    this._router.get("/", (req, resp) => { 
      resp.send("POLICY CONTROLLER (GET)")   
    });


    this._router.get('/getpoliciesbyusername/:name',  (req, resp) => {
      this._loginService.isAuthentificated(req, resp, "admin").then(res => {
        if (res.ok) {
          let name = req.params.name;

          this._service.getPoliciesByUserName(name).then(res => {
            resp.send(res); 
          }).catch(error => {
            resp.send(error);  
          });
        } else {
          resp.send(res);
        }
      }).catch(error => {
        resp.send(error);
      });
    });

    this._router.get('/:id',  (req, resp) => {
      this._loginService.isAuthentificated(req, resp, "admin").then(res => {
        if (res.ok) {
          let id = req.params.id;

          if (id == "all") {
            this._service.getAll().then(res => {
              resp.send(res); 
            }).catch(error => {
              resp.send(error);  
            });
          } else {
            this._service.get(id)
              .then(res => {
                resp.send(res); 
              })
              .catch(error => {
                resp.send(error);
              });
          }
        } else {
          resp.send(res);
        }
      }).catch(error => {
        resp.send(error);
      });
    });
          
  }
}
