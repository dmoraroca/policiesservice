import { Router } from "express";
import { ClientService } from "../service/client.service";
import { LoginService } from "../service/login.service";
import { Client } from "../pocos/client.poco";

export class ClientController
{
    private _router: Router
    private _service: ClientService = new ClientService();
    private _loginService = new LoginService()
        
    /**
     * Initialize the HeroRouter
     */
    constructor() {
      this._router = Router();
      this.setRouterController();
      
    }

    /**
     * 
     * get router for client.
     * 
    */
    GetRouter() {
      return this._router;
    }


    /**
     /Data zone
     */
    private setRouterController() {      
      /**
       * test if client api working for POST
       */
      this._router.post("/", (req, resp) => { 
        resp.send("CLIENT CONTROLLER OK (POST)")   
      });

      /**
       * test if client api working for GET
       */
      this._router.get("/", (req, resp) => { 
        resp.send("CLIENT CONTROLLER OK (GET)")   
      });

      /**
       * filter by user id
       */
      this._router.get('/:id',  (req, resp) => {        
        this._loginService.isAuthentificated(req, resp, "user,admin").then(res => {
          if (res.ok) {
            let id: string = req.params.id;

            if (id == "all") {
              this._service.getAll().then(res => {
                resp.send(res); 
              }).catch(error => {
                resp.send(error);  
              });          
            } else {        
              this._service.get(id).then(res => {
                resp.send(res); 
              }).catch(error => {            
                resp.send(error);            
              });          
            }
          } else {
            resp.send(res); 
          }
        });
      });

      /**
       * filter by user name
       */
      this._router.get('/getbyname/:name',  (req, resp) => {
        this._loginService.isAuthentificated(req, resp, "user,admin").then(res => {
          if (res.ok) {
            let name: string = req.params.name;       

            this._service.getByName(name).then(res => {
              resp.send(res); 
            })
            .catch(error => {
              resp.send(error);
            });
          } else {
            resp.send(res); 
          }
        });
      });
    }
}
